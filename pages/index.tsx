import type { NextPage } from "next"
import Image from "next/image"
import styles from "../styles/Home.module.css"
import { useMoralisQuery, useMoralis } from "react-moralis"
import { useEffect, useState } from "react"
import NFTBox from "../components/NFTBox"
import GET_ACTIVE_ITEM from "../constants/subgraphQueries"
import { useQuery } from "@apollo/client"

const Home: NextPage = () => {
  // const [page, setPage] = useState(1)
  const { isWeb3Enabled } = useMoralis()
  // const marketplaceAddress = (networkMapping as NetworkConfigMap)[chainString].NftMarketplace[0]
  const marketplaceAddress = "0x204DA0Cb23ee397C8F4338EE1306d3F9D2d30063"
  const { loading, error, data: listedNfts } = useQuery(GET_ACTIVE_ITEM)

  console.log(listedNfts)

  if (error) {
    console.log(`fetching error: ${error}`)
  } else {
    console.log(`All Ok! No Fetching Error Found`)
  }

  return (
    <div className="container mx-auto">
      <h1 className="py-4 px-4 font-bold text-2xl">Recently Listed</h1>
      <div className="flex flex-wrap">
        {isWeb3Enabled ? (
          loading || !listedNfts ? (
            <div>Loading...</div>
          ) : (
            listedNfts.activeItems.map((nft: any) => {
              const { price, nftAddress, tokenId, seller } = nft

              return (
                <NFTBox
                  price={price}
                  nftAddress={nftAddress}
                  tokenId={tokenId}
                  marketplaceAddress={marketplaceAddress}
                  seller={seller}
                  key={`${nftAddress}${tokenId}`}
                />
              )
            })
          )
        ) : (
          <div>Web3 Currently Not Enabled </div>
        )}
      </div>
    </div>
  )
}

export default Home
